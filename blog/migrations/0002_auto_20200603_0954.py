# Generated by Django 3.0.6 on 2020-06-03 01:54

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='date_posted',
            field=models.DateTimeField(default=datetime.datetime(2020, 6, 3, 1, 54, 10, 318184, tzinfo=utc)),
        ),
    ]
