from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView,DetailView,CreateView,UpdateView,DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin,UserPassesTestMixin
from django.contrib.auth.models import User
from .models import Groupbuy,Product

posts=[
    {
     'author'       :   'CoreyMS',
     'title'        :   'Blog Post 1',
     'content'      :   'First post content',
     'date_posted'  :   'August 27, 2018'
    },
    {
     'author'       :   'Jane Doe',
     'title'        :   'Blog Post 2',
     'content'      :   'Second post content',
     'date_posted'  :   'August 28, 2019'
    }    
]

def home(request):
    context = {
        'posts' : Groupbuy.objects.all()
    }
    return render(request,'pasabuy/home.html', context)

class UserProductListView(ListView):
    model=Product
    template_name='pasabuy/user_products.html'
    # <app>/<model>_<viewtype>.html
    context_object_name='products'
    paginate_by = 5

    def get_queryset(self):
        user=get_object_or_404(User,username=self.kwargs.get('username'))
        return Product.objects.filter(owner=user).order_by('-date_posted')

class ProductDetailView(DetailView):
    model=Product

class ProductUpdateView(LoginRequiredMixin,UserPassesTestMixin,UpdateView):
#loginrequiredmixin will prohibit you from viewing and creating a new post if your logged out
    model=Product
    fields=['image','name']
    # success_url = '/blog/' - use this is you want to redirect via a class based redirect
    def form_valid(self,form):
        form.instance.owner=self.request.user
        return super().form_valid(form)
    def test_func(self):
        product = self.get_object()
        if self.request.user == product.owner:
            return True
        return False    

class ProductDeleteView(LoginRequiredMixin,UserPassesTestMixin,DeleteView):
    model=Product
    success_url = 'blog'
    def test_func(self):
        post = self.get_object()
        if self.request.user == product.owner:
            return True
        return False