# Generated by Django 3.0.7 on 2020-06-13 05:49

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('pasabuy', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='groupbuy',
            name='date_posted',
            field=models.DateTimeField(default=datetime.datetime(2020, 6, 13, 5, 49, 2, 419179, tzinfo=utc)),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('price', models.DecimalField(decimal_places=2, max_digits=6)),
                ('image', models.ImageField(default='default.jpg', upload_to='product_pics')),
                ('date_posted', models.DateTimeField(default=datetime.datetime(2020, 6, 13, 5, 49, 2, 419679, tzinfo=utc))),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
