from django.urls import path
from .views import (
    UserProductListView,
    ProductDetailView,
    ProductUpdateView,
    ProductDeleteView,
)
from . import views

urlpatterns = [
    path('', views.home, name='index'),
    path('user/<str:username>', UserProductListView.as_view(), name='user-products'),
    #path('user/<str:username>', UserPostListView.as_view(), name='user-posts'),
    path('product/<int:pk>/', ProductDetailView.as_view(), name='product-detail'),
    path('product/<int:pk>/update', ProductUpdateView.as_view(), name='product-update'),
    path('product/<int:pk>/delete', ProductDeleteView.as_view(), name='product-delete'),
    #path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    #path('<int:question_id>/vote/', views.vote, name='vote'),
]